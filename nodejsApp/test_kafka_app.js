var WebSocketServer = require('ws').Server
wss = new WebSocketServer({ port: 8034 });

wss.broadcast = function broadcast(data) {
      console.log('sending to all: %s', data)
      wss.clients.forEach(function each(client) {
              client.send(JSON.stringify(data));
                });
};


var kafka = require('kafka-node');
    Consumer = kafka.Consumer;
    
    client = new kafka.Client("localhost:2181", "test")
    consumer = new Consumer(
            client,
            [
                { topic: 'test', partition: 0 }
            ],
            {
                autoCommit: false
            }
       );


consumer.on('message', function (message) {
      console.log(message);
        wss.broadcast(message);
});


