import java.util.Properties

import kafka.producer._
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.sql.catalyst.expressions._
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.sql.cassandra.CassandraSQLContext
import org.joda.time.DateTime
import org.joda.time.DateTime._
import org.joda.time.format.DateTimeFormatter
import org.joda.time.format.DateTimeFormat._
import org.joda.time.format.DateTimeFormat
import org.apache.spark.streaming.{Seconds, StreamingContext}
import com.datastax.spark.connector.streaming._
import com.datastax.spark.connector._

object ExcoDriver {
    def main(args: Array[String]) {
        if (args.length < 2) {
            System.err.println("Usage: CustomReceiver <hostname> <port>")
            System.exit(1)
        } 
        

        //set log level to warn..dont want too much stuff on console 
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)
              
        //set up sparkconf
        val sparkConf = new SparkConf().setAppName("ExcoReceiver").set("spark.cassandra.connection.host", "127.0.0.1")
        val ssc = new StreamingContext(sparkConf, Seconds(1))
        //receive the data
        val lines = ssc.receiverStream(new CustomReceiver(args(0), args(1).toInt))
        val rdd = lines.map(CassandraUtil.makeRecord(_))
        //save to cassandra
        rdd.saveToCassandra("exco","original")
        //slice something for further processing
        val numbArray  = lines.map(_.split(",").slice(10,15).map(_.toDouble))
        val vec = numbArray.transform(rdd=> rdd.map(Vectors.dense(_)))
        
        //for each rdd in inputstream containg double arrays send to kafka
        numbArray.foreachRDD{ rdd => rdd.foreach{arr =>
            //set up kafka properties
            val props = new Properties()
            props.put("metadata.broker.list", "Hermione:9092");
            props.put("serializer.class", "kafka.serializer.StringEncoder")
            props.put("key.serializer.class", "kafka.serializer.StringEncoder")
            val producer = new Producer[String, String](new ProducerConfig(props))
            //for each double array convert it to a string and send it to a kafka
           
                val message = arr(0).toString
                println(message)
                val data = new KeyedMessage[String,String]("test", message)
                producer.send(data)
            }
        }
        ssc.start()
        ssc.awaitTermination()
    }
}
