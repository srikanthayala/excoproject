import socket
#import sys
import time

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
host = socket.gethostname()                           
port = 1235                                          
serversocket.bind((host, port))
serversocket.listen(5) # listen for 5 concurrent request
print "Listening for client . . ."
clientsocket,addr = serversocket.accept()      
print("Got a connection from %s" % str(addr))

while True:
#    currentTime = time.ctime(time.time()) + "\r\n"
    lines = [line.strip() for line in open('testData/CompleteScadaData.csv')] # Reading of file start here
    lines = lines[2:]
    for l in lines:
         clientsocket.send(l)
         clientsocket.send("\n")
         time.sleep(.5) # for given seconds it will wait to emit next line

clientsocket.close()
